# theme-project10k

## Overview

A Project 10K theme for Chrome browser.

## Maintainers

Primary: @msound

## Reference

A good source of documentation about Chrome themes:

https://github.com/Patrick-Batenburg/GoogleChromeThemeCreationGuide
